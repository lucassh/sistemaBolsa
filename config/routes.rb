Rails.application.routes.draw do

  get 'documents/show'
  get 'projects/index' => 'projects#index', as: :projects_index
  
  resources :projects

  devise_for :admins, 
  controllers: {sessions: "admins/sessions"}

  # Routes for devise admin
  devise_scope :admin do
    get '/login' => 'admins/sessions#new', as: :login
  end

  # Routes for admin-projects       
  authenticated :admin do
    get '/admin/index' => 'admin#index', as: :admin_index
    get '/admin' => 'admin#index', as: :admin_root
    #Route for generate PDF
    get ':id/document.pdf' => 'documents#show', as: 'generate_pdf'
    # Route for update departament field
    get 'update_departaments' => 'projects#update_departaments', as: 'update_departaments'
    root 'admin#index'
  end


  root 'home#index'
  #root 'projects#index'

 
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
