class Departament < ApplicationRecord
  has_one :place, :foreign_key => 'Loc_Cod',
                  :primary_key => 'Dep_Local'
end
