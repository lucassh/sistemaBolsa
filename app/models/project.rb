class Project < ApplicationRecord
  has_one :coordinator, foreign_key: 'Ori_Nome',
  						primary_key: 'coordinator_name'
  validates :coordinator_name, :title, :subject_area, :academic_unit, :departament, :abstract, :keyword1, :keyword2, :keyword3, :keyword4, :start_date, :end_date, :money_approved, :pocket, presence: true

  serialize :pocket, Array
end
