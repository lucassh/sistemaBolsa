require 'prawn'
class Document
  attr_accessor :project
  attr_accessor :path

  PDF_OPTIONS = {
    :page_size   => "A4",
    :page_layout => :portrait
  }

  def initialize(project, path = nil)
    
    @project = project
    @path = path
  end

  def pdf
    Prawn::Document.new(PDF_OPTIONS) do |pdf|
      pdf.font "Times-Roman"
      pdf.fill_color "000000"
      pdf.text "#{@project.coordinator_name.upcase}", :inline_format => true, :align => :center, :size => 18 
      pdf.text "#{@project.academic_unit.upcase}", :inline_format => true, :align => :center, :size => 18
      pdf.text "#{@project.departament.upcase}", :inline_format => true, :align => :center, :size => 18
      pdf.text "#{@project.subject_area.upcase}", :inline_format => true, :align => :center, :size => 18
      pdf.text "#{@project.departament.upcase}", :inline_format => true, :align => :center, :size => 18
      pdf.text "    "
      pdf.text "#{@project.title.titleize}", :inline_format => true, :align => :center, :size => 18
      pdf.text "#{@project.abstract.capitalize}", :inline_format => true, :align => :justify, :size => 12
      pdf.text "#{@project.keyword1.capitalize} | #{@project.keyword2.capitalize} | #{@project.keyword3.capitalize} | #{@project.keyword4.capitalize}", :inline_format => true, :align => :center, :size => 12
    end
  end

  def render
    #pdf.render_file(path) #salvar aquivo em tmp
    pdf.render #retornar "source"
  end

  private
  def artigo
    if @current_user.gender == "Feminino"
      'a'
    else
      'o'
    end
  end
  #
end




 