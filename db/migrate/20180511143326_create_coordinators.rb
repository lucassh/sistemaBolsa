class CreateCoordinators < ActiveRecord::Migration[5.1]
  def change
    create_table :coordinators do |t|
      t.string :Ori_Cpf
      t.string :Ori_Nome
      t.string :Ori_Email
      t.string :Ori_Celular
      t.string :Ori_EndLattes
      t.integer :Ori_Ppg
      t.string :Ori_Vinculo
      t.integer :Ori_Status

      t.timestamps
    end
  end
end
