class CreateDepartaments < ActiveRecord::Migration[5.1]
  def change
    create_table :departaments do |t|
      t.integer :Dep_Cod
      t.string :Dep_descricao
      t.string :Dep_Local
      t.integer :Dep_Status

      t.timestamps
    end
  end
end
